package rhsummitlabs2019;

import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.WebRequest;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.util.UrlUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.ActiveProfiles;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@ActiveProfiles("sidecar-auth")
@AutoConfigureMockMvc
class SideCarAuthApplicationTest {

  @Autowired
  WebClient webClient;

  @Test
  void greetsUserByFirstAndLastName() throws IOException {
    HttpHeaders headers = new HttpHeaders();
    headers.add("username", "summituser");
    headers.add("firstname", "Summit");
    headers.add("lastname", "User");
    headers.add("groups", "employee;developer");

    WebRequest request = new WebRequest(UrlUtils.toUrlUnsafe("http://localhost:8080/"));
    request.setAdditionalHeaders(headers.toSingleValueMap());

    HtmlPage page = webClient.getPage(request);

    assertThat(page.getBody().getTextContent()).contains("Hello Summit User!");
  }

  @Test
  void demosRolesFromSso() throws IOException {
    HttpHeaders headers = new HttpHeaders();
    headers.add("username", "summituser");
    headers.add("groups", "employee;developer");

    WebRequest request = new WebRequest(UrlUtils.toUrlUnsafe("http://localhost:8080/"));
    request.setAdditionalHeaders(headers.toSingleValueMap());

    HtmlPage page = webClient.getPage(request);

    assertThat(page.getBody().getTextContent()).contains("Roles: developer, employee");
  }

  @Test
  void displaysEmail() throws IOException {
    HttpHeaders headers = new HttpHeaders();
    headers.add("username", "summituser");
    headers.add("email", "summituser@summit.test");

    WebRequest request = new WebRequest(UrlUtils.toUrlUnsafe("http://localhost:8080/"));
    request.setAdditionalHeaders(headers.toSingleValueMap());

    HtmlPage page = webClient.getPage(request);

    assertThat(page.getBody().getTextContent()).contains("Email: summituser@summit.test");
  }
}
