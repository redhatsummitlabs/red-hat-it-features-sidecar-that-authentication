package rhsummitlabs2019;

import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.WebRequest;
import com.gargoylesoftware.htmlunit.html.HtmlButton;
import com.gargoylesoftware.htmlunit.html.HtmlForm;
import com.gargoylesoftware.htmlunit.html.HtmlInput;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.util.UrlUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@AutoConfigureMockMvc
class LoginAuthApplicationTest {

  @Autowired
  WebClient webClient;

  @AfterEach
  void logout() throws IOException {
    WebRequest request = new WebRequest(UrlUtils.toUrlUnsafe("http://localhost:8080/logout"));

    HtmlPage logout = webClient.getPage(request);
    HtmlButton confirm = (HtmlButton) logout.getForms().get(0).getElementsByTagName("button").get(0);
    HtmlPage loggedOut = confirm.click();

    assertThat(loggedOut.getTitleText()).isEqualTo("Please sign in");
  }

  @Test
  void greetsUserByUsernameAfterLoggingIn() throws IOException {
    HttpHeaders headers = new HttpHeaders();

    WebRequest request = new WebRequest(UrlUtils.toUrlUnsafe("http://localhost:8080/"));
    request.setAdditionalHeaders(headers.toSingleValueMap());

    HtmlPage loginPage = webClient.getPage(request);

    assertThat(loginPage.getTitleText()).isEqualTo("Please sign in");

    HtmlForm loginForm = (HtmlForm) loginPage.getElementsByTagName("form").get(0);

    HtmlInput username = loginForm.getInputByName("username");
    HtmlInput password = loginForm.getInputByName("password");
    HtmlButton submit = (HtmlButton) loginForm.getElementsByTagName("button").get(0);

    username.type("admin");
    password.type("r3dh4t1!");
    HtmlPage welcome = submit.click();

    assertThat(welcome.getBody().getTextContent()).contains("Hello admin!");
  }

  @Test
  void doesNotAllowLoginWithUnknownCredentials() throws IOException {
    HttpHeaders headers = new HttpHeaders();

    WebRequest request = new WebRequest(UrlUtils.toUrlUnsafe("http://localhost:8080/"));
    request.setAdditionalHeaders(headers.toSingleValueMap());

    HtmlPage loginPage = webClient.getPage(request);

    assertThat(loginPage.getTitleText()).isEqualTo("Please sign in");

    HtmlForm loginForm = (HtmlForm) loginPage.getElementsByTagName("form").get(0);

    HtmlInput username = loginForm.getInputByName("username");
    HtmlInput password = loginForm.getInputByName("password");
    HtmlButton submit = (HtmlButton) loginForm.getElementsByTagName("button").get(0);

    username.type("admin");
    password.type("foo");
    HtmlPage afterLogin = submit.click();

    assertThat(afterLogin.getTitleText()).isEqualTo("Please sign in");
    assertThat(afterLogin.getBody().getTextContent()).doesNotContain("Hello admin!");
  }
}
