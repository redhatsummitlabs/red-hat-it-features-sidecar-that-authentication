package rhsummitlabs2019;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.core.env.Profiles;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.AuthenticationUserDetailsService;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationProvider;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.security.web.authentication.preauth.RequestHeaderAuthenticationFilter;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Collectors;

@SpringBootApplication
public class Application {
  public static void main(String[] args) {
    SpringApplication.run(Application.class, args);
  }

  @Controller
  public static class ApplicationConfig {

    @GetMapping("/")
    public String handle(Model model) {
      SecurityContext context = SecurityContextHolder.getContext();
      AppUser user = (AppUser) context.getAuthentication().getPrincipal();
      model.addAttribute("fullname", user.getFullName());
      model.addAttribute("roles", user.getAuthorities().stream()
          .map(GrantedAuthority::getAuthority)
          .collect(Collectors.joining(", ")));
      model.addAttribute("username", user.getUsername());
      model.addAttribute("email", user.getEmail());
      return "index";
    }
  }

  @Configuration
  @Profile("default")
  public static class LoginSecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
      http.formLogin()
          .and()
          .authorizeRequests()
          .anyRequest().authenticated()
          .mvcMatchers("/error").permitAll();
    }

    @Bean
    @Override
    public UserDetailsService userDetailsService() {
      UserDetails user = User.withDefaultPasswordEncoder()
          .username("admin")
          .password("r3dh4t1!")
          .roles()
          .build();

      InMemoryUserDetailsManager inMemory = new InMemoryUserDetailsManager(user);
      return username -> new AppUser((User) inMemory.loadUserByUsername(username));
    }
  }

  @Configuration
  @Profile("sidecar-auth")
  public static class SideCarSecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private Environment environment;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
      PreAuthenticatedAuthenticationProvider preAuthProvider = new PreAuthenticatedAuthenticationProvider();
      // See AuthenticationDetailsSource set below
      preAuthProvider.setPreAuthenticatedUserDetailsService(new HeaderBasedUserDetailsService());

      auth.authenticationProvider(preAuthProvider);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
      // TODO require header secret?
      RequestHeaderAuthenticationFilter headerAuth = new RequestHeaderAuthenticationFilter();
      headerAuth.setAuthenticationDetailsSource(req -> new ServletServerHttpRequest(req).getHeaders());
      headerAuth.setPrincipalRequestHeader(environment.getProperty("principal-request-header", "username"));
      headerAuth.setAuthenticationManager(authenticationManager());

      http.addFilter(headerAuth)
          .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
          .and()
          .authorizeRequests()
          .anyRequest().authenticated()
          .mvcMatchers("/error").permitAll();
    }

    private static class HeaderBasedUserDetailsService implements
        AuthenticationUserDetailsService<PreAuthenticatedAuthenticationToken> {
      private static final Logger log = LoggerFactory.getLogger(HeaderBasedUserDetailsService.class);

      @Override
      public UserDetails loadUserDetails(PreAuthenticatedAuthenticationToken token)
          throws UsernameNotFoundException {
        HttpHeaders headers = (HttpHeaders) token.getDetails();

        log.info("headers={}", headers);

        return new AppUser.Builder()
            .setEmail(headers.getFirst("email"))
            .setFirstName(headers.getFirst("firstname"))
            .setLastName(headers.getFirst("lastname"))
            .setUsername(token.getName())
            .setPassword("N/A")
            .setGroups(Arrays.stream(
                Optional.ofNullable(
                    headers.getFirst("groups")).map(g -> g.split(";"))
                    .orElse(new String[0]))
                .collect(Collectors.toSet()))
            .build();
      }
    }
  }
}
