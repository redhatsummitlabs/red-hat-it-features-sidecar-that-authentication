package rhsummitlabs2019;

import org.springframework.lang.Nullable;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class AppUser extends User {
  @Nullable
  private final String firstName;
  @Nullable
  private final String lastName;
  @Nullable
  private final String email;

  public AppUser(User user) {
    super(user.getUsername(), user.getPassword(),
        user.isEnabled(), user.isAccountNonExpired(), user.isCredentialsNonExpired(),
        user.isAccountNonLocked(), user.getAuthorities());
    firstName = null;
    lastName = null;
    email = null;
  }

  private AppUser(Builder builder) {
    super(builder.username, builder.password, builder.groups.stream()
        .map(SimpleGrantedAuthority::new)
        .collect(Collectors.toSet()));
    firstName = builder.firstName;
    lastName = builder.lastName;
    email = builder.email;
  }

  public static class Builder {
    private String username;
    private String password;
    private String firstName;
    private String lastName;
    private String email;
    private Set<String> groups;

    public Builder setUsername(String username) {
      this.username = username;
      return this;
    }

    public Builder setPassword(String password) {
      this.password = password;
      return this;
    }

    public Builder setGroups(Set<String> groups) {
      this.groups = groups;
      return this;
    }

    public Builder setFirstName(String firstName) {
      this.firstName = firstName;
      return this;
    }

    public Builder setLastName(String lastName) {
      this.lastName = lastName;
      return this;
    }

    public Builder setEmail(String email) {
      this.email = email;
      return this;
    }

    public AppUser build() {
      return new AppUser(this);
    }
  }

  public String getFullName() {
    return Stream.of(firstName, lastName)
        .filter(Objects::nonNull)
        .collect(Collectors.collectingAndThen(
            Collectors.joining(" "),
            n -> n.isEmpty() ? getUsername() : n));
  }

  @Nullable
  public String getFirstName() {
    return firstName;
  }

  @Nullable
  public String getLastName() {
    return lastName;
  }

  @Nullable
  public String getEmail() {
    return email;
  }
}
