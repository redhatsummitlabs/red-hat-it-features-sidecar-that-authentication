#!/bin/bash
set -x
P="/opt/ca"
cp ${P}/myca.crt /etc/pki/ca-trust/source/anchors/
update-ca-trust --extract
